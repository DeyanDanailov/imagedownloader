const stream = require('stream');
const {promisify} = require('util');
const fs = require('fs');
const got = require('got');

const pipeline = promisify(stream.pipeline);

async function downloadImage(url, name) {
    await pipeline(
        got.stream(url),
        fs.createWriteStream(name)
    );
}

// (async () => {
//     await downloadImage('https://example.com/test.jpg', 'test.jpg');
// })();

let url = 'https://app-static.earth2.io/assets/flags/__.png'

async function downloadAllImages(url, arrayOfCountryCodes) {
    const downloads = [];
    let picUrl;
    for (let i = 0; i < arrayOfCountryCodes.length; i++) {
        picUrl = url.replace('__', arrayOfCountryCodes[i].toLowerCase());
        downloads.push(
            await downloadImage(picUrl, `./downloadedPics/${arrayOfCountryCodes[i].toLowerCase()}.png`)
        );

    }
}

const countryCodes = ["__", "AE-AZ", "AE-AJ", "AE-DU", "AE-FU", "AE-RK", "AE-SH", "AE-UQ", "US", "IT", "GB", "DE", "AU", "KR", "CA", "FR", "CN", "JP", "ES", "NR", "MX", "CH", "NL", "IN", "RU", "GR", "BR", "SG", "ZA", "TH", "SE", "AT", "TR", "BE", "NZ", "NO", "ID", "RO", "CL", "EG", "PT", "TW", "DK", "VN", "PH", "QA", "IE", "BS", "PE", "PL", "MV", "HR", "VE", "HK", "AR", "MY", "MA", "KW", "CD", "IS", "FI", "CO", "MR", "NP", "CU", "DO", "BH", "PA", "HU", "KM", "CZ", "BO", "CY", "MD", "KP", "NG", "MT", "UA", "MG", "FJ", "CR", "LU", "PK", "LB", "GI", "BW", "JM", "TL", "RS", "KZ", "EC", "LK", "TZ", "AQ", "BG", "EE", "JE", "GH", "OM", "TN", "MU", "PG", "VC", "GL", "KY", "IM", "AL", "LT", "UZ", "BD", "LY", "SI", "MK", "GT", "DZ", "KE", "NA", "UY", "MN", "LV", "SK", "ZW", "PW", "UG", "YE", "ME", "TC", "FO", "LR", "LI", "AZ", "BY", "KH", "BZ", "AF", "BM", "NI", "SM", "SC", "SN", "HN", "VG", "FK", "SB", "LA", "PS", "SL", "PY", "GD", "BI", "BB", "SY", "BA", "AO", "MM", "JO", "DJ", "TG", "CI", "LS", "TD", "NE", "MZ", "VU", "ZM", "SZ", "GG", "BN", "ET", "GQ", "SV", "CK", "GN", "HT", "SR", "GM", "KN", "MH", "SO", "SD", "BF", "GE", "KI", "TT", "ML", "KG", "HM", "AM", "MW", "WS", "CG", "LC", "GY", "TM", "AD", "CV", "CM", "NF", "DM", "GA", "BT", "IO", "TO", "AG", "NU", "BJ", "TJ", "SS", "MC", "SH", "AI", "MS", "CF", "ST", "EH", "ER", "GW", "TV", "PN", "FM", "RW", "GS"];


const imagemin = require('imagemin');
const imageminWebp = require('imagemin-webp');

(async () => {
    await imagemin(['downloadedPics/*.{jpg,png}'], {
        destination: 'images',
        plugins: [
            imageminWebp({quality: 50})
        ]
    });

    console.log('Images optimized');
})();